#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */ 
    board = Board();
    currentside = side;
    if (currentside == WHITE){
        otherside = BLACK;
    } else if (currentside == BLACK){
        otherside = WHITE;
    }
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */

    board.doMove(opponentsMove, otherside);
    float mostPoints = 0; //the move that would give you the biggest difference in your favor
    Move* bestMove = NULL; //best move to return

    for(int i = 0; i<8; i++)
    {
        for(int j = 0; j<8; j++)
        {
            Move* move = new Move(i,j);                                 
            if (board.checkMove(move, currentside))
            {

                Board* tempboard = board.copy();
                float initial = tempboard->count(currentside) - tempboard->count(otherside);
                tempboard->doMove(move, currentside);
                float final = tempboard->count(currentside) - tempboard->count(otherside);
                float points = final - initial;
                if(i == 0 && j == 0 || i == 7 && j == 7 || i == 0 && j == 7 || i == 7 && j == 0)
                {
                    points = points * 10;
                }
                else if(i == 0 && j == 1 || i == 1 && j == 0 || i == 6 && j == 0 || i == 7 && j == 1 || i == 0 && j == 6 || i == 1 && j == 7 || i == 6 && j == 7 || i == 7 && j == 6)
                {
                    points = points/5;
                }
                else if(i == 1 && j == 1 || i == 1 && j == 6 || i == 6 && j == 1 || i == 6 && j == 6)
                {
                    points = points/10;
                }
                else if(i == 0 || i == 7 || j == 0 || j == 7)
                {
                    points = points * 5;
                }
                if(points > mostPoints)
                {
                    mostPoints = points;
                    bestMove = move;
                }
            }

            
        }
    }

    if(mostPoints > 0 && bestMove != NULL)
    {
        board.doMove(bestMove, currentside);
        return bestMove;
    }
    return NULL;
}
    
