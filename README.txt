Contribution:

We both created the bitbucket and tested hw to use Git to modify and turn in files for sharing, including committing and pushing.

Jerry typed up most of the constructor and doMove's functionalities while Nancy helped revise the code and point out syntax and logical errors. Then they switched roles and Nancy typed up most of the heuristic algorithm with Jerry fixed some bugs and added a few more functions to it.

First, we created a working othello player with basic funcitonality that iterated through all possible moves, found the ones that were legal, and made the first legal move. Then, we added the heuristic algorithm which measured the change in difference in points between the player and the opponent, and made the move that maximized that score. Then, we added some statements that made corners more valuable, since they are strategically important, and made moves that opened up corners to opponents much less valuable. After a several tests, we found that our player beat constant time player 8 out of 10 times, and simpleplayer nearly all the time. 
